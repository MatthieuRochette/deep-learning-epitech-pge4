# deep-learning-epitech-pge4

13/12/2021

## Authors
- Clément Chalopin
- Matthieu Rochette
- Alexandre Guichet

## Description
In this project we use the fashion-MNIST dataset to train a deep learning AI model to recognise clothing articles.
It is a famous dataset pre-included in tensorflow, created by the Zalando research teams.
This dataset contains images of 28x28 pixels, corresponding to 10 labels.
The goal of this model is to be able to classify the images from this dataset into their corresponding labels, thus allowing it to recognize real clothes from pictures.

## Usage

Requires Python 3.8+, Pip and Git LFS

Clone the repository:
`git clone https://gitlab.com/MatthieuRochette/deep-learning-epitech-pge4.git`

Pull from Git LFS:
```bash
git lfs init
git lfs pull
```

```bash
pip install -r requirements.txt
streamlit run ./streamlit_app.py
```

It should automatically open a web browser page. The model might take a while to load.
You can provide the AI images of your own, or use the examples provided in the `assets test/real-life images/` folder.
