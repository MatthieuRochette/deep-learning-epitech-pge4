FROM python:3.7-slim-bullseye

ENV STREAMLIT_SERVER_HEADLESS true
ENV STREAMLIT_GLOBAL_DEVELOPMENT_MODE false
ENV STREAMLIT_SERVER_ALLOW_RUN_ON_SAVE false

WORKDIR /app
COPY . .

RUN apt-get update
RUN apt-get install -y libgl1-mesa-dev libglib2.0-0
RUN pip install -r requirements.txt

EXPOSE 8501

CMD [ "streamlit", "run", "./streamlit_app.py" ]