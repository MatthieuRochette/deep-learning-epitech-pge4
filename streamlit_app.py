import io
import os
from tempfile import TemporaryDirectory


import cv2
import numpy as np
import streamlit as st
import matplotlib.pyplot as plt
from PIL import Image, ImageOps
from tensorflow import keras

from deep_learning_fashion_mnist_dataset_epitech_full import *

cls_names_arr = np.array(class_names, dtype=object)

# CONFIGURATIONS
# =============================================================================
np.set_printoptions(
    edgeitems=30, linewidth=100000, formatter=dict(float=lambda x: "%.3g" % x)
)

# CONSTANTS
# =============================================================================
DEFAULT_MODEL_SAVE_PATH = "./saved_model"
TEMP_DIR = TemporaryDirectory()
FIG = plt.figure(figsize=(4, 2))

# model initialisation functions
# =============================================================================
def try_load_model(path: str = DEFAULT_MODEL_SAVE_PATH):
    """
    Load a saved model if available, otherwise return None.
    """
    if os.path.isdir(path) and os.listdir(path):  # If directory exists and is not empty
        model = keras.models.load_model(path)
        return model
    return None


def load_or_create_model(path: str = DEFAULT_MODEL_SAVE_PATH):
    """
    Load (if possible) or create/compile/train/save a model
    """
    model = try_load_model()
    if model is None:
        model = create_model()
        model = compile_model_with_adam(model)
        model = fit_model(model, epochs=10)
        model.save(path)
    return model


# streamlit initialisation
# =============================================================================
st.title("Fashion-MNIST Deep Learning model")
with st.spinner(
    """The model is being trained or the page is being reloaded.
In the first case, this might take a long time. Sorry for the inconvenience."""
):
    # model initialisation
    # =========================================================================
    model = load_or_create_model()
    probability_model = tf.keras.Sequential([model, tf.keras.layers.Softmax()])
    # Evaluate the model to showcase it's efficacity later in the UI
    score = model.evaluate(test_images, test_labels, verbose=0)

# streamlit main page content
# =============================================================================
st.text("""The model is ready !""")
st.text("You can see here how this model performs in a fashion-MNIST evaluation:")
st.markdown(
    f"""
|Score| (in %)|
|:----|------:|
|Accuracy:|**{score[1] * 100:4.4f}**|
|Loss:|**{score[0] * 100:4.4f}**|"""
)
st.text("The clothing categories that the model tries to predict are the following:")
str_classes = ""
for i, item in enumerate(class_names):
    str_classes += str(i) + ") " + item + "\n"
st.text(str_classes + "\n\n")

# streamlit file input widget
# =============================================================================
st.subheader("Upload your own images to test with this Deep Learning model")
uploaded = st.file_uploader(
    label="Please upload images of clothing articles with a transparent, black or white background.",
    type=["png", "jpg"],
    accept_multiple_files=True,
)


# prediction results container functions
# =============================================================================
def remove_img_background(image: Image.Image):
    # load image
    img = np.array(image.convert("RGB"))
    img = img[:, :, ::-1].copy()

    # convert to graky
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # threshold input image as mask
    mask = cv2.threshold(gray, 230, 255, cv2.THRESH_BINARY)[1]

    # negate mask
    mask = 255 - mask

    # apply morphology to remove isolated extraneous noise
    # use borderconstant of black since foreground touches the edges
    kernel = np.ones((3, 3), np.uint8)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

    # anti-alias the mask -- blur then stretch
    # blur alpha channel
    mask = cv2.GaussianBlur(
        mask, (0, 0), sigmaX=2, sigmaY=2, borderType=cv2.BORDER_DEFAULT
    )

    # linear stretch so that 127.5 goes to 0, but 255 stays 255
    mask = (2 * (mask.astype(np.float32)) - 255.0).clip(0, 255).astype(np.uint8)

    # put mask into alpha channel
    result = img.copy()
    result = cv2.cvtColor(result, cv2.COLOR_BGR2BGRA)
    result[:, :, 3] = mask

    # save resulting masked image
    save_path = os.path.join(TEMP_DIR.name, "background_removed.png")
    cv2.imwrite(save_path, result)
    image.close()
    image = Image.open(save_path)
    return image


def check_pixel_black_or_transparent(pixel) -> bool:
    if isinstance(pixel, int):
        return pixel == 255
    elif isinstance(pixel, tuple) and len(pixel) == 4:
        return pixel[3] == 0
    elif isinstance(pixel, tuple) and len(pixel) == 3:
        return pixel == (0, 0, 0)
    else:
        return False


def pad_image(image: Image.Image) -> Image.Image:
    old_size = image.size  # old_size is in (width, height) format
    desired_size = max(old_size)
    delta_w = desired_size - old_size[0]
    delta_h = desired_size - old_size[1]
    if old_size[0] < old_size[1]:
        padding = (
            delta_w // 2,
            delta_h // 2,
            delta_w - (delta_w // 2),
            delta_h - (delta_h // 2),
        )
    else:
        padding = (
            delta_w - (delta_w // 2),
            delta_h - (delta_h // 2),
            delta_w // 2,
            delta_h // 2,
        )
    new_im = ImageOps.expand(image, padding, fill=(0, 0, 0, 0))
    image.close()
    return new_im


def resize_image_keep_ratio(image: Image.Image, basewidth: int = 80) -> Image.Image:
    wpercent = basewidth / float(image.size[0])
    hsize = int((float(image.size[1]) * float(wpercent)))
    return image.resize((basewidth, hsize), Image.ANTIALIAS)


def process_image(file) -> dict:
    # create a PIL.Image object from the image's bytes
    image = Image.open(io.BytesIO(file.getvalue()))
    copy_original_img = image.copy()

    # remove background if not black or transparent
    top_left_pixel = image.getpixel((0, 0))
    if not check_pixel_black_or_transparent(top_left_pixel):
        image = remove_img_background(image)

    # Modify the image to fit the model's format requirements
    if image.size[0] != 28 or image.size[1] != 28:
        # Resize to fit the model's needs
        if image.size[0] != image.size[1]:
            image = pad_image(image)
        image = image.resize((28, 28))
    # convert to grayscale
    image_grayscale = ImageOps.grayscale(image)

    # get pixel values and convert the flat list to a 2D table, then to an ndarray
    pixels = np.array(list(image_grayscale.getdata()))
    pixels_np_arr = pixels.reshape((28, 28))

    # Convert values from a [0, 255] integers range to a [0, 1] float range
    pixels_np_arr = pixels_np_arr / 255.0

    return {
        "file_name": file.name,
        "image": copy_original_img,
        "pixels_map": pixels_np_arr,
    }


def plot_predictions_array(file_name, predicted_label, predictions_array):
    plt.xticks(range(10))
    bar_plot = plt.bar(range(10), predictions_array, color="#777777")
    bar_plot[predicted_label].set_color("blue")
    path = os.path.join(TEMP_DIR.name, file_name[:-4] + "-chart.png")
    plt.savefig(path)
    plt.clf()
    return path


def create_expander(processed_image):
    with st.expander(processed_image["file_name"], expanded=True):
        processed_image["image"] = resize_image_keep_ratio(processed_image["image"])
        st.image(
            processed_image["image"],
            caption="Prediction: This image contains a " + processed_image["class"],
        )
        st.image(
            Image.open(
                plot_predictions_array(
                    processed_image["file_name"],
                    processed_image["cls_index"],
                    processed_image["predictions"],
                )
            ),
            caption="Probabilities determined by the model for this prediction",
        )


# predict class for each uploaded image
# =============================================================================
if uploaded:
    # process the images to make them into an acceptable format for the model
    # and get metadata like the file name to print on the screen later
    i = 1
    progress_bar = st.progress(0)
    for file in uploaded:
        try:
            processed_image = process_image(file)

            # get prediction values and predicted class of the image
            processed_image["predictions"] = probability_model.predict(
                np.array([processed_image["pixels_map"]])
            )[0]
            processed_image["cls_index"] = np.argmax(processed_image["predictions"])
            processed_image["class"] = class_names[processed_image["cls_index"]]

            # create the container to show in the UI
            create_expander(processed_image)

        except Exception as e:
            with st.expander(file.name, expanded=True):
                st.error(
                    "This image encountered and error while being processed and predicted. Please excuse us."
                )
                st.exception(e)

        finally:
            # update progress bar
            progress_bar.progress(i * 100 // len(uploaded))
            i += 1
